/** Requereimos el fichero de configuración */
require("./config/config");

const express = require("express");
const app = express();

/** Para leer el body de las peticiones neceistamos el paquete body-parser */
const bodyParser = require("body-parser");

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

/** app.use son funciones middleware, esto son funciones que se van a disparar cada vez que pase por aquí el código */

app.get("/usuarios", function(req, res) {
    res.status(200).json("getUsuario");
});

app.post("/usuarios", function(req, res) {
    let body = req.body;

    if (body.nombre === undefined) {
        res.status(400).json({
            ok: false,
            mensaje: "El nombre es necesario",
        });
    } else {
        res.status(201).json({
            persona: body,
        });
    }
});

app.put("/usuarios/:id", function(req, res) {
    let id = req.params.id;

    res.status(200).json({
        id,
    });
});

app.delete("/usuarios/:id", function(req, res) {
    res.status(204).json("deleteUsuario");
});

app.listen(process.env.PORT, () =>
    console.log(`Escuchando el puerto ${process.env.PORT}`)
);