# REST Server

## Descripción

Instalamos la dependencia de Express.

Configuramos el server para la lectura de las peticiones.
Instalamos el paquete Body-Parser para la lectura del body de las peticiones.

Se incluyen los códigos de respuesta con la funcion de express res.status(code).

Creamos un archivo de configuración global para los distintos entornos.

Incluimos el script start en el package.json para que la aplicación se pueda desplegar en Heroku.

# Conceptos nuevos

# Repaso de conceptos
