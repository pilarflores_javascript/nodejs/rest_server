# Temas puntuales de la sección
Temas de la sección: 

Aquí cubriremos varios temas como: 

- Instalación y pruebas con MongoDB
- Peticiones HTTP
- Get
- Put
- Post
- Delete
- Aprender sobre códigos de error HTTP
- Códigos de error en Express
- Archivos para la configuración global
- Tips importantes en Postman